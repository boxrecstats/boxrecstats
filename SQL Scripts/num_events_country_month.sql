﻿SELECT COUNT(eventID) AS NumEvents, strftime('%m-%Y',e.eventDate) AS [month-year], v.venAddress AS Country
       FROM event e JOIN venue v ON v.venueId=e.venueId
       WHERE eventDate>'2015-01-01'
       GROUP BY [month-year], Country
       ORDER BY Country, e.eventDate
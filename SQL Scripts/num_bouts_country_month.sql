﻿SELECT COUNT(b.boutId) AS numBouts, strftime('%Y-%m',e.eventDate) AS [yearMonth], v.venAddress AS country
       FROM event e 
            JOIN venue v 
                 ON v.venueId=e.venueId
            JOIN bout b
                 ON e.eventId=b.eventId
       WHERE eventDate>'2015-01-01'
       GROUP BY [yearMonth], country
       ORDER BY country, e.eventDate
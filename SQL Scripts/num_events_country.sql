﻿SELECT COUNT(e.eventId) AS numEvents,v.venAddress AS Country
       FROM event e JOIN venue v ON e.venueId=v.venueId
       WHERE e.eventDate>'2015-01-01' AND e.eventDate<'2016-01-01'
       GROUP BY v.venAddress
       ORDER BY numEvents DESC
       LIMIT 20
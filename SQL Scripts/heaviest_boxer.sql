﻿SELECT boxer.name AS name, bout.boutId AS boutId, bout.bxr1Weight AS [weight], 'bxr1Weight' FROM bout JOIN boxer
ON boxer.boxerId=bout.bxr1Id
WHERE bout.bxr1Weight=(SELECT MAX(bxr1Weight) FROM bout WHERE bxr1Weight<1106)
UNION
SELECT boxer.name AS name, bout.boutId AS boutId, bout.bxr2Weight AS [weight], 'bxr2Weight' FROM bout JOIN boxer
ON boxer.boxerId=bout.bxr2Id
WHERE bout.bxr2Weight=(SELECT MAX(bxr2Weight) FROM bout WHERE bxr2Weight<1106)
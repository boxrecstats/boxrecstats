﻿SELECT COUNT(*) AS numBoxers, resRegion AS Country FROM boxer WHERE Country IS NOT NULL GROUP BY resRegion
UNION
SELECT COUNT(*) AS numBoxers, bpRegion AS Country FROM boxer WHERE resRegion IS NULL AND bpRegion IS NOT NULL GROUP BY bpRegion
ORDER BY numBoxers DESC
import re
import scrapy
from datetime import date, timedelta, datetime

class boxrecSpider(scrapy.Spider):
	name = "event"
	start_urls = ["http://boxrec.com/login"]

	def parse(self,response):
		return [scrapy.FormRequest.from_response(
			response,
			formxpath='id("loginform")',
			formdata={'username':'jimgonzales','password':'jimgonz'},
			callback=self.after_login)]

	def after_login(self, response):
		if b"authentication failed" in response.body:
			self.log("Login failed", level=log.ERROR)
			return
		else: #successful login
			###---------------Set THIS---------###
			year1='1957'
			year2='1959'
			start_date=datetime.strptime(year1+'-01-01','%Y-%m-%d').date()
			end_date=datetime.strptime(year2+'-12-31','%Y-%m-%d').date()
			days_to_add=(end_date-start_date).days+1
			###--------------------------------###
			other_urls=[]
			for i in range(days_to_add):
				d=start_date+timedelta(days=i) #doesn't include end_date
				url='http://boxrec.com/calendar?v=d&d='+str(d)
				other_urls.append(url)
			for m in range(len(other_urls)):
				url=other_urls[m]
				#asynchronously send the results to parse_page as they come in
				request=scrapy.Request(url, callback=self.go_to_event)
				request.meta['date']=url.split("=")[-1]
				request.meta['progress']=m*100/len(other_urls)
				yield request

	def go_to_event(self, response):
		self.log('Read page %s' % response.url)

		#The following Xpath will produce an error (and will end the parsing for this date) if there is no event on this date
		table=response.xpath("/html/body/div[@id='pageContent']/div[@id='homeOuter']/div[@id='homeContent']/div[@class='contentC']/div[@id='page_content']/div[@style='overflow:hidden;']/div[@style='clear:both;']/table[@class='tBoutListTable']")[0]
		event_urls=table.xpath("./tr[following-sibling::*[position()=1][name()='tbody']]//a[@title='event']/@href").extract()
		for url in event_urls:
			request=scrapy.Request(url, callback=self.parse_page)
			#request=scrapy.Request('http://boxrec.com/show/740090', callback=self.parse_page) #for specific requests
			request.meta['date']=response.meta['date']
			request.meta['progress']=response.meta['progress']
			yield request

	def parse_page(self,response):
		#write the html to file
		"""eventNum=response.url.split("/")[-1]
		filename = 'event_%s.html' % eventNum
		
		with open(filename, 'wb') as f:
			f.write(response.body)"""
			
		self.log('Read page %s' % response.url)

		#Just a sub section of the HTML code to make parsing simpler
		bBox=response.xpath("/html/body/div[@id='pageContent']/div[@id='homeOuter']/div[@id='homeContent']/div[@itemtype='http://schema.org/SportsEvent']/div[@class='contentC']/div[@id='page_content']/div[@class='bBox']")[0]
		
		#EVENT INFORMATION
		event={'event_pg':response.url.split("/")[-1],'date': response.meta['date'],'v_name':'','v_pg':'', 'v_loc':'', 'v_reg':'','v_addr':'', 'bouts':[], 'other_list':[], 'person_list':[]} #event item
		
		event['v_name']=bBox.xpath("./div[@class='bgShow']/div[@itemtype='http://schema.org/Place']/div/b/a/span/text()").extract_first(default="")
		event['v_pg']=bBox.xpath("./div[@class='bgShow']/div[@itemtype='http://schema.org/Place']/div/b/a//@href").extract_first(default='/').split("/")[-1] #should return an empty string if no value with this?
		event['v_loc']=bBox.xpath("./div[@class='bgShow']/div[@itemtype='http://schema.org/Place']/b/div[@itemprop='address']/span[@itemprop='addressLocality']/text()").extract_first(default="") #street?
		event['v_reg']=bBox.xpath("./div[@class='bgShow']/div[@itemtype='http://schema.org/Place']/b/div[@itemprop='address']/span[@itemprop='addressRegion']/text()").extract_first(default="") #city
		event['v_addr']=bBox.xpath("./div[@class='bgShow']/div[@itemtype='http://schema.org/Place']/b/div[@itemprop='address']/span[@itemprop='streetAddress']/text()").extract_first(default="") #country
		
		#EVENT PERSONS AND OTHER ASSOCIATIONS: Promoters, doctors, TV channels, etc.
		event_table=bBox.xpath("./div[@class='bgShow']/div[3]/table/tr")
		for tr in event_table:
			person_dict={'person_pg':'','role':'','name':'', 'note':''}
			other_dict={'role':'','name':''}
			if tr.xpath('./self::*[@itemtype="http://schema.org/Person"]').extract_first() is not None:	
				link_temp=''
				link_temp=tr.xpath("./td[2]/a/@href").extract_first(default='')
				person_dict['person_pg']=link_temp.split("/")[-1].split("?")[0]
				person_dict['role']=link_temp.split("role=")[-1]
				person_dict['name']=tr.xpath('./td[2]/a/span[1]/span[1]/text()').extract_first(default="")+' '+tr.xpath('./td[2]/a/span[1]/span[2]/text()').extract_first(default="")
				person_dict['note']=tr.xpath("./td[2]/a/following-sibling::text()[position()=1]").extract_first(default="")
				person_dict['note']=person_dict['note'].rstrip('\xa0| ').rstrip(',').lstrip('\xa0| ')
				event['person_list'].append(person_dict)
			else:
				other_dict['role']=tr.xpath("./td[1]/text()").extract_first(default="")
				other_dict['name']=tr.xpath("./td[2]/text()").extract_first(default="")
				event['other_list'].append(other_dict)

		#BOUTS
		bouts=bBox.xpath("./div[3]/table[@class='tBoutListTable']/tbody")

		self.log('number of bouts for event: %s' % str(len(bouts)))

		for bout in bouts:
			bout_holder={'query':'','cls':'','bxr1_pg':'','bxr1_name':'','bxr1_wgt':'','bxr1_W':'','bxr1_L':'','bxr1_D':'','bxr1_last6':[],'WLS':'','bxr2_pg':'','bxr2_name':'',
			'bxr2_wgt':'','bxr2_W':'','bxr2_L':'','bxr2_D':'','bxr2_last6':[],'ofcl_rslt':'','ft_vs_sched':'','rtg_pct':'','title_list':[],'person_list':[],'notes':[],'bout_pg':''} #dict for bout
			
			bout_holder['query']=bout.xpath("./tr[1]/td[1]/div[1]/@class").extract_first(default='')
			bout_holder['cls']=bout.xpath("./tr[1]/td[2]/text()").extract_first(default='')
			
			#BOXER 1
			bout_holder['bxr1_pg']=bout.xpath("./tr[1]/td[3]/a/@href").extract_first(default='/').split("/")[-1] #splits the unique code from the address, defaults to blank string #works
			bout_holder['bxr1_name']=bout.xpath("./tr[1]/td[3]/a/text()").extract_first(default='TBA')
			bout_holder['bxr1_wgt']=bout.xpath("./tr[1]/td[4]/text()").extract_first(default='')
			bout_holder['bxr1_W']=bout.xpath("./tr[1]/td[5]/span[1]/b/text()").extract_first(default='')
			bout_holder['bxr1_L']=bout.xpath("./tr[1]/td[5]/span[2]/b/text()").extract_first(default='')
			bout_holder['bxr1_D']=bout.xpath("./tr[1]/td[5]/span[3]/b/text()").extract_first(default='')
			#last_6 1
			bxr1_last6=[]
			for k in range(6):
				bxr1_last6.append(bout.xpath("./tr[1]/td[6]/div[1]/div["+str(k+1)+"]/@class").extract_first(default='_').split("_")[-1])
			bout_holder['bxr1_last6']=bxr1_last6
			
			bout_holder['WLS']=bout.xpath("./tr[1]/td[7]//text()").extract_first(default='')

			#BOXER 2
			bout_holder['bxr2_pg']=bout.xpath("./tr[1]/td[8]/a/@href").extract_first(default='/').split("/")[-1]
			bout_holder['bxr2_name']=bout.xpath("./tr[1]/td[8]/a/text()").extract_first(default='TBA')
			bout_holder['bxr2_wgt']=bout.xpath("./tr[1]/td[9]/text()").extract_first(default='')
			bout_holder['bxr2_W']=bout.xpath("./tr[1]/td[10]/span[1]/b/text()").extract_first(default='')
			bout_holder['bxr2_L']=bout.xpath("./tr[1]/td[10]/span[2]/b/text()").extract_first(default='')
			bout_holder['bxr2_D']=bout.xpath("./tr[1]/td[10]/span[3]/b/text()").extract_first(default='')
			#last_6 2
			bxr2_last6=[]
			for k in range(6):
				bxr2_last6.append(bout.xpath("./tr[1]/td[11]/div[1]/div["+str(k+1)+"]/@class").extract_first(default='_').split("_")[-1])
			bout_holder['bxr2_last6']=bxr2_last6

			#OFFICIAL RESULT---from here on will not work with events that haven't yet happened
			bout_holder['ofcl_rslt']=bout.xpath("./tr[1]/td[12]/span[1]/text()").extract_first(default='')
			#Rounds fought vs rounds scheduled
			bout_holder['ft_vs_sched']=bout.xpath("./tr[1]/td[13]/span[1]/text()").extract_first(default='')
			#Some sort of star rating for the bout
			bout_holder['rtg_pct']=bout.xpath("./tr[1]/td[14]/span[1]/span[1]/@style").extract_first(default=" ;").split()[-1][:-2] #split at the whitespace and ignore the semi-colon at the end of the string
			
			#Bout page
			bout_holder['bout_pg']=bout.xpath("./tr[1]/td[15]/div[2]/a/@href").extract_first(default='/').split("/")[-1]

			#Notes on the bout including titles (more than one possible), important persons and technical details
			note=''
			for des in bout.xpath("./tr[3]/td//*"):
				title_dict={'acronym':'','division':'','supervisor_pg':'','name':''} #title dictionary
				person_dict={'person_pg':'','role':'','name':'', 'note':''} #person dictionary for bout

				link_temp=''
				link_temp=des.xpath("./@href").extract_first(default='') #doesn't necessarily exist
				if "/person/" in link_temp:
					person_dict['person_pg']=link_temp.split("/")[-1].split("?")[0]
					person_dict['role']=link_temp.split("role=")[-1]
					person_dict['name']=des.xpath('./text()').extract_first()
					if person_dict['role']=='judge':	#not required for supervisor or referee
						person_dict['note']=des.xpath("./following-sibling::text()[position()=1]").extract_first(default="") #score
						person_dict['note']=person_dict['note'].rstrip('\xa0').rstrip(',').lstrip('\xa0')
					if person_dict['role']=='supervisor' and len(bout_holder['title_list'])>0: #this is a hacky way of attributing a supervisor to the last title
						bout_holder['title_list'][-1]['supervisor_pg']=person_dict['person_pg']
					bout_holder['person_list'].append(person_dict)

				elif "/title/" in link_temp:
					title_dict['acronym']=link_temp.split("/")[-1].split("?")[0]
					title_dict['division']=link_temp.split("division=")[-1]
					title_dict['name']=des.xpath('./text()').extract_first()

					bout_holder['title_list'].append(title_dict)
			
			#combine all of the text in the note row; breaks interpreted as separaters of distinct notes
			for span in bout.xpath("./tr[3]/td/child::*"):
				note='';note_part=''
				span_length=len(span.xpath("./node()"))
				for l in range(span_length):
					note_part=span.xpath("./node()["+str(l+1)+"][self::text()]").extract() + span.xpath("./node()["+str(l+1)+"]/text()").extract()
					if span.xpath("./node()["+str(l+1)+"][self::br]").extract_first() is None and len(note_part)>0:
						note+=note_part[0]
						if l==span_length-1:
							bout_holder['notes'].append(note.replace('\xa0',' '))
					elif span.xpath("./node()["+str(l+1)+"][self::br]").extract_first() is not None:
						bout_holder['notes'].append(note.replace('\xa0',' '))
						note='';note_part=''

			event['bouts'].append(bout_holder)
			
		yield {'event':event}
		#self.log('Progress: '+str(response.meta['progress'])+'%')
		#10 Days: 16->17s
		#2016: 11min
		#2015
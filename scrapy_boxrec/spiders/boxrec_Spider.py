import re
import scrapy
from datetime import date, timedelta

class boxrecSpider(scrapy.Spider):
	name = "schedule"
	start_urls = ["http://boxrec.com/login"]

	def parse(self,response):
		return [scrapy.FormRequest.from_response(
			response,
			formxpath='id("loginform")',
			formdata={'username':'gimzy','password':'spoonerman'},
			callback=self.after_login)]

	def after_login(self, response):
		if b"authentication failed" in response.body:
			self.log("Login failed", level=log.ERROR)
			return
		else: #successful login
			###---------------Set THIS---------###
			days_to_subtract=30
			###--------------------------------###
			other_urls=[]
			for i in range(days_to_subtract):
				d=date.today()-timedelta(days=i+1) #doesn't include today's date
				url='http://boxrec.com/calendar?v=d&d='+str(d)
				other_urls.append(url)
			for url in other_urls:
				#asynchronously send the results to parse_page as they come in
				yield scrapy.Request(url, callback=self.parse_page) 

	def parse_page(self,response):
		self.log('Read page %s' % response.url)
		"""dater=response.url.split("d=")[-1]
		filename = 'schedule_%s.html' % dater
		
		with open(filename, 'wb') as f:
			f.write(response.body)"""

		table=response.xpath("/html/body/div[@id='pageContent']/div[@id='homeOuter']/div[@id='homeContent']/div[@class='contentC']/div[@id='page_content']/div[@style='overflow:hidden;']/div[@style='clear:both;']/table[@class='tBoutListTable']")[0]
				
		#EVENTS
		event_markers=table.xpath("./tr[following-sibling::*[position()=1][name()='tbody']]") #only tr's followed immediately by tbody's
		self.log('%s events' % len(event_markers))
		

		for i in range(len(event_markers)):
			#in same row
			event={'date': re.search("\d{4}\-\d{2}-\d{2}",response.url).group(0),'flag':'','loc':'','loc_href':'', 'fights':[]}
			
			event['flag']=event_markers[i].xpath('./td[@class="bgShow bLeft bRight bTop"]/div/div/div[contains(@class,"flag")]/@title').extract_first()
			event['loc']=event_markers[i].xpath('./td[@class="bgShow bLeft bRight bTop"]/div/div/b/a/text()').extract_first()
			event['loc_href']=event_markers[i].xpath('./td[@class="bgShow bLeft bRight bTop"]/div/div/b/a/@href').extract_first(default='/').split("/")[-1] #should return an empty string if no value with this?

			#FIGHTS
			#the following code grabs all of the fights for a given event
			#direct paths were the only way I was able to apply Kayessian intersection for some reason
			tablePath="/html/body/div[@id='pageContent']/div[@id='homeOuter']/div[@id='homeContent']/div[@class='contentC']/div[@id='page_content']/div[@style='overflow:hidden;']/div[@style='clear:both;']/table[@class='tBoutListTable']"
			k1=tablePath+'/child::tr['+str(2*i+1)+']/following-sibling::*' #first tbody in group to the last following sibling  V
			k2=tablePath+'/child::tr['+str(2*i+2)+']/preceding-sibling::*' #last tbody in group to the last preceding sibling   ^
			fights=response.xpath(k1+'[count(.|'+k2+') = count('+k2+')]') #keyessian Xpath intersection ><

			self.log('number of fights for event: '+str(len(fights)))

			for fight in fights:
				#dict for the particular fight
				fight_holder={'query':'','cls':'','bxr1_pg':'','bxr1_name':'','bxr1_W':'','bxr1_L':'','bxr1_D':'','bxr1_last6':[],'WLS':'','bxr2_pg':'','bxr2_name':'',
				'bxr2_W':'','bxr2_L':'','bxr2_D':'','bxr2_last6':[],'ofcl_rslt':'','ft_vs_sched':'','rtg_pct':'','title_dict':[],'notes':[],'bout_pg':''}
				
				fight_holder['query']=fight.xpath("./tr[1]/td[1]/div[1]/@class").extract_first() #actually unsure what this icon signifies
				fight_holder['cls']=fight.xpath("./tr[1]/td[2]/text()").extract_first(default='')
				
				#BOXER 1
				fight_holder['bxr1_pg']=fight.xpath("./tr[1]/td[3]/a/@href").extract_first(default='/').split("/")[-1] #splits the unique code from the address, defaults to blank string
				fight_holder['bxr1_name']=fight.xpath("./tr[1]/td[3]/a/text()").extract_first(default='TBA')
				fight_holder['bxr1_W']=fight.xpath("./tr[1]/td[4]/span[1]/b/text()").extract_first(default='')
				fight_holder['bxr1_L']=fight.xpath("./tr[1]/td[4]/span[2]/b/text()").extract_first(default='')
				fight_holder['bxr1_D']=fight.xpath("./tr[1]/td[4]/span[3]/b/text()").extract_first(default='')
				#last_6 1
				bxr1_last6=[]
				for k in range(6):
					bxr1_last6.append(fight.xpath("./tr[1]/td[5]/div[1]/div["+str(k+1)+"]/@class").extract_first(default='_').split("_")[-1]) #
				fight_holder['bxr1_last6']=bxr1_last6
				
				fight_holder['WLS']=fight.xpath("./tr[1]/td[6]//text()").extract_first()

				#Boxer 2
				fight_holder['bxr2_pg']=fight.xpath("./tr[1]/td[7]/a/@href").extract_first(default='/').split("/")[-1]
				fight_holder['bxr2_name']=fight.xpath("./tr[1]/td[7]/a/text()").extract_first(default='TBA')
				fight_holder['bxr2_W']=fight.xpath("./tr[1]/td[8]/span[1]/b/text()").extract_first(default='')
				fight_holder['bxr2_L']=fight.xpath("./tr[1]/td[8]/span[2]/b/text()").extract_first(default='')
				fight_holder['bxr2_D']=fight.xpath("./tr[1]/td[8]/span[3]/b/text()").extract_first(default='')
				#last_6 2
				bxr2_last6=[]
				for k in range(6):
					bxr2_last6.append(fight.xpath("./tr[1]/td[9]/div[1]/div["+str(k+1)+"]/@class").extract_first(default='_').split("_")[-1])
				fight_holder['bxr2_last6']=bxr2_last6

				#Official Result---from here on will not work with events that haven't yet happened
				fight_holder['ofcl_rslt']=fight.xpath("./tr[1]/td[10]/span[1]/text()").extract_first(default='')
				#Rounds fought vs rounds scheduled
				fight_holder['ft_vs_sched']=fight.xpath("./tr[1]/td[11]/span[1]/text()").extract_first(default='')
				#Some sort of star rating
				fight_holder['rtg_pct']=fight.xpath("./tr[1]/td[12]/span[1]/span[1]/@style").extract_first(default=" ;").split()[-1][:-2] #split at the whitespace and ignore the semi-colon at the end of the string
				
				#All important bout page
				fight_holder['bout_pg']=fight.xpath("./tr[1]/td[13]/div[2]/a/@href").extract_first(default='/').split("/")[-1]

				#Notes on the fight including titles (more than one possible) and technical details
				for a in fight.xpath("./tr[3]/td/a"):
					title_dict={'title':'','title_pg':''}
					title_dict['title']=a.xpath("./text()").extract_first()
					title_dict['title_pg']=a.xpath("./@href").extract_first(default='/').split("/")[-1]
					fight_holder['title_dict'].append(title_dict)
					
				fight_holder['notes']=fight.xpath("./tr[3]/td/span//text()").extract()
				
				event['fights'].append(fight_holder)
			
			yield {'event':event}
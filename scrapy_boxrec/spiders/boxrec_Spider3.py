import scrapy

class boxrecSpider(scrapy.Spider):
	name = 'boxer'
	start_urls = ["http://boxrec.com/login"]

	def parse(self,response):
		return [scrapy.FormRequest.from_response(
			response,
			formxpath='id("loginform")',
			formdata={'username':'jimgonzales','password':'jimgonz'},
			callback=self.after_login)]
	
	def after_login(self, response):
		if b"authentication failed" in response.body:
			self.log("Login failed", level=log.ERROR)
			return
		else: #successful login
			start=750001
			end=780725
			#max_fornow=780725
			for i in range(start,end+1):
				url="http://boxrec.com/boxer/"+str(i)
				yield scrapy.Request(url, callback=self.parse_page)

	def parse_page(self,response):
		if b"authentication failed" in response.body:
			self.log("Login failed", level=log.ERROR)
			return
		elif response.xpath(
			'''/html/body/div[@id="pageContent"]/div[@id="homeOuter"]/div[@id="homeContent"]
			/div[@class="colContainerFull"]/div[@class="colContainerSmall"]/div[@class="colBig"][@class="contentC"]/div[@class="errorBox"]'''
			).extract_first() is None and response.xpath(
			'''/html/body/div[@id="pageContent"]/div[@id="homeOuter"]/div[@id="homeContent"]/div[@class="colContainerFull"]/div[@class="colContainerSmall"]/div[@class="colBig"]/div[@class="contentC"]
			/div[2]/div[contains(@id,"bo")]/div[2]/table[@itemtype="http://schema.org/Person"]
			/tr/td[1]/child::*[text()="role"]/parent::*/following-sibling::*[1][contains(text(),"Boxer")]'''
			).extract_first() is not None: #make sure there is a person with that ID and make sure it is a boxer
			
			"""boxerNum=response.url.split("/")[-1]
			filename = 'boxer_%s.html' % boxerNum
			#rating,ranking global,ranking local,rankingregion,bouts, roungs, KOpercent, globalId,USId,titleAcronym,titledivision, birthname,alias,birthdate,debut,division,stance,height,reach,residence,birthplace,manager,promoter
			
			with open(filename, 'wb') as f:
				f.write(response.body)"""
				
			self.log('Read page %s' % response.url)
			boxer={'name':'','boxrecRating':'','rankingGlobal':'','rankingLocal':'','rankingArea':'','numBouts':'','numRounds':'','KOpercent':'','globalId':'',
				'birthName':'','UsId':'','titleList':[],'birthName':'','alias':'','birthDate':'','debut':'','division':'',
				'deathDate':'','stance':'','height':'','reach':'','resLoc':'','resAddr':'','resReg':'','brtLoc':'','brtAddr':'',
				'brtReg':'','manager_pg':'','promoter_pg':'','gender':'','imageUrl':''}

			contentC=response.xpath('/html/body/div[@id="pageContent"]/div[@id="homeOuter"]/div[@id="homeContent"]/div[@class="colContainerFull"]/div[@class="colContainerSmall"]/div[@class="colBig"]/div[@class="contentC"]')[0]

			boxer['name']=contentC.xpath('./div[@class="contentTitle"]/h1[@class="boxerTitle"]/text()').extract_first(default='').rstrip().lstrip()

			boxer['imageUrl']=contentC.xpath('.//img[@id="profile_image"]/@src').extract_first(default="") #works, could be improved

			table=contentC.xpath('./div[2]/div[contains(@id,"bo")]/div[2]/table[@itemtype="http://schema.org/Person"]')[0]

			boxer['gender']=table.xpath('./tr/td[1]/meta[@itemprop="gender"]/@content').extract_first(default="") #works

			for a in table.xpath('./tr/td[1]/child::*[text()="titles held"]/parent::*/following-sibling::*[1]/a'):#works
				title_dict={'acronym':'','division':'','name':''}
				title_dict['acronym']=a.xpath('./@href').extract_first('/?').split("/")[-1].split("?")[0]
				title_dict['division']=a.xpath('./@href').extract_first('=').split("=")[-1]
				title_dict['name']=a.xpath('./text()').extract_first(default="")
				boxer['titleList'].append(title_dict)

			
			boxer['alias']=table.xpath('./tr/td[1]/child::*[text()="alias"]/parent::*/following-sibling::*[1]/span/text()').extract_first(default="")

			boxer['boxrecRating']=table.xpath('./tr/td[1]/child::*[text()="rating"]/parent::*/following-sibling::*[1]/div/span/span/@style').extract_first(default=" ;").split()[-1][:-2]#works

			boxer['rankingGlobal']=table.xpath('./tr/td[1]/child::*[contains(text(),"ranking")]/parent::*/following-sibling::*[1]/div[2]/a/text()').extract_first(default="").rstrip().lstrip()#works
			boxer['rankingLocal']=table.xpath('./tr/td[1]/child::*[contains(text(),"ranking")]/parent::*/following-sibling::*[1]/div[4]/a/text()').extract_first(default="").rstrip().lstrip()#works
			boxer['rankingArea']=table.xpath('./tr/td[1]/child::*[contains(text(),"ranking")]/parent::*/following-sibling::*[1]/div[3]/@class').extract_first(default='flag ').split('flag ')[-1]#works

			boxer['numBouts']=table.xpath('./tr/td[1]/child::*[text()="bouts"]/parent::*/following-sibling::*[1]/text()').extract_first(default="")#works
			boxer['numRounds']=table.xpath('./tr/td[1]/child::*[text()="rounds"]/parent::*/following-sibling::*[1]/text()').extract_first(default="")#works
			boxer['KOpercent']=table.xpath('./tr/td[1]/child::*[text()="KOs"]/parent::*/following-sibling::*[1]/text()').extract_first(default="")#works
			boxer['globalId']=table.xpath('./tr/td[1]/child::*[text()="global ID"]/parent::*/following-sibling::*[1]/text()').extract_first(default="")#works
			boxer['USId']=table.xpath('./tr/td[1]/child::*[text()="US ID"]/parent::*/following-sibling::*[1]/text()').extract_first(default="")#works
			boxer['birthName']=table.xpath('./tr/td[1]/child::*[text()="birth name"]/parent::*/following-sibling::*[1]//text()').extract_first(default="")#works
			boxer['birthDate']=table.xpath('./tr/td[1]/child::*[text()="born"]/parent::*/following-sibling::*[1]/span/text()').extract_first(default="")#works
			boxer['deathDate']=table.xpath('./tr/td[1]/child::*[text()="death date"]/parent::*/following-sibling::*[1]/span/text()').extract_first(default="")#works
			boxer['debut']=table.xpath('./tr/td[1]/child::*[text()="debut"]/parent::*/following-sibling::*[1]/text()').extract_first(default="")#works
			boxer['division']=table.xpath('./tr/td[1]/child::*[text()="division"]/parent::*/following-sibling::*[1]//text()').extract_first(default="")#works
			boxer['stance']=table.xpath('./tr/td[1]/child::*[text()="stance"]/parent::*/following-sibling::*[1]/text()').extract_first(default="")#works
			boxer['height']=table.xpath('./tr/td[1]/child::*[text()="height"]/parent::*/following-sibling::*[1]//text()').extract_first(default="\xa0 ").split("\xa0 ")[-1]#works
			boxer['reach']=table.xpath('./tr/td[1]/span/child::*[text()="reach"]/parent::span/parent::td/following-sibling::*[1]//text()').extract_first(default="\xa0 ").split("\xa0 ")[-1]#works

			boxer['resLoc']=table.xpath('./tr/td[1]/child::*[text()="residence"]/parent::*/following-sibling::*[1]/span/div/span[@itemprop="addressLocality"]/text()').extract_first(default="")#works
			boxer['resAddr']=table.xpath('./tr/td[1]/child::*[text()="residence"]/parent::*/following-sibling::*[1]/span/div/span[@itemprop="addressRegion"]/text()').extract_first(default="")#works
			boxer['resReg']=table.xpath('./tr/td[1]/child::*[text()="residence"]/parent::*/following-sibling::*[1]/span/div/span[@itemprop="streetAddress"]/text()').extract_first(default="")#works
			boxer['brtLoc']=table.xpath('./tr/td[1]/child::*[text()="birth place"]/parent::*/following-sibling::*[1]/span/div/span[@itemprop="addressLocality"]/text()').extract_first(default="")#works
			boxer['brtAddr']=table.xpath('./tr/td[1]/child::*[text()="birth place"]/parent::*/following-sibling::*[1]/span/div/span[@itemprop="addressRegion"]/text()').extract_first(default="")#works
			boxer['brtReg']=table.xpath('./tr/td[1]/child::*[text()="birth place"]/parent::*/following-sibling::*[1]/span/div/span[@itemprop="streetAddress"]/text()').extract_first(default="")#works

			boxer['manager_pg']=table.xpath('./tr/td[1]/child::*[text()="manager/agent"]/parent::*/following-sibling::*[1]/a/@href').extract_first(default="/?").split('/')[-1].split('?')[0]#works
			boxer['promoter_pg']=table.xpath('./tr/td[1]/child::*[text()="promoter"]/parent::*/following-sibling::*[1]/a/@href').extract_first(default="/?").split('/')[-1].split('?')[0]

			



			yield {'boxer':boxer}